# http://www.braeunig.us/space/constant.htm
from qb import qb
import plotly.io as pio
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import numpy as np


def estSignPnts(cb, dyn = 10, reg = 10):
    """  Sub-sample points to keep number of points sane
    
    Based on dynamics and a regular grid to fil the gaps.
    
    Args:
        cb:    cubeSat class
        dyn:   Percentage of points with high dynamics.
        reg:   Per thousand of regular grid points
    Returns:
        Index point
    
    Example:
        If N is number of total points, length of return index would be 
        (dyn / 100) * N  points selected based on higher dynamics
        + (reg / 1000) * N based on a regular grid
        Returned list would have less points since some may coincide 
    """
    wNrm = np.linalg.norm(cb.wCbI[:, 0: cb.k], axis = 0)
    dWNrm = np.abs(np.diff(wNrm, prepend = [0]))
    pX = np.arange(0,101,1)
    a = np.percentile(dWNrm, pX)
    # Select only point with high dynamics
    idx1 = np.where(dWNrm > a[int(100 - dyn)])[0]
    
    # Regular grid 
    idx2 = np.arange(0, cb.k, int(1000 / reg))
    
    #Combine
    idx = np.hstack((idx1,idx2)) 
    idx = np.sort(idx)
    idx = np.unique(idx)
    return idx

def plotFigs(cb, fnm, show = False, idx = None):
    
    if idx is None:
        # Good luck.. 
        idx = np.arange(0, len(cb.t))
    
    # Plot disturbance and control torques 
    fig = make_subplots(rows = 3, cols = 1,
                        shared_xaxes = True,
                        subplot_titles = ("Gravity Gradient", " Aerodynamic", "Control"),
                        vertical_spacing = 0.05)
    
    fig.layout.update(title="Torques (Disregarding enable signals)") #paper_bgcolor='rgba(0,0,0,0)', plot_bgcolor='rgba(0,0,0,0)')
    fig.add_trace(go.Scatter(x = cb.t[idx], y = cb.GgTcb[0, idx], mode="lines", showlegend = True, name = 'X'), row=1, col=1)
    fig.add_trace(go.Scatter(x = cb.t[idx], y = cb.GgTcb[1, idx], mode="lines", showlegend = True, name = 'Y'), row=1, col=1)
    fig.add_trace(go.Scatter(x = cb.t[idx], y = cb.GgTcb[2, idx], mode="lines", showlegend = True, name = 'Z'), row=1, col=1)
    
    fig.add_trace(go.Scatter(x = cb.t[idx], y = cb.AdTcb[0, idx], mode="lines", showlegend = True, name = 'X'), row=2, col=1)
    fig.add_trace(go.Scatter(x = cb.t[idx], y = cb.AdTcb[1, idx], mode="lines", showlegend = True, name = 'Y'), row=2, col=1)
    fig.add_trace(go.Scatter(x = cb.t[idx], y = cb.AdTcb[2, idx], mode="lines", showlegend = True, name = 'Z'), row=2, col=1)
    
    fig.add_trace(go.Scatter(x = cb.t[idx], y = cb.cTCb[0, idx], mode="lines", showlegend = True, name = 'X'), row=3, col=1)
    fig.add_trace(go.Scatter(x = cb.t[idx], y = cb.cTCb[1, idx], mode="lines", showlegend = True, name = 'Y'), row=3, col=1)
    fig.add_trace(go.Scatter(x = cb.t[idx], y = cb.cTCb[2, idx], mode="lines", showlegend = True, name = 'Z'), row=3, col=1)
    
    fig.update_yaxes(title_text="Gravity Gradient Torque, (Nm)", row = 1, col = 1)
    fig.update_yaxes(title_text="Aerodynamic Torque, (Nm)", row = 2, col = 1)
    fig.update_yaxes(title_text="Control Torque, (Nm)", row = 3, col = 1)
    fig.update_xaxes(title_text="Time (s)", row = 3, col = 1)
    if show:
        fig.show()
    htmlFnm = fnm.split('.', 2)[0] + 'Trqs.html'
    pio.write_html(fig, htmlFnm)
    
    # Plot solar equivalent areas for power estimation
    fig = go.Figure()
    lbl = ['X', 'Y', 'Z', '-X', '-Y', '-Z']
    for i in range(6):
        fig.add_trace(go.Scatter(x = cb.t[idx], y = cb.solEqvFct[i, idx], mode='lines', name=lbl[i]))
    fig.layout.update(title = "Solar equivalent area per face as percentage of the installed PV panels")
    fig.update_yaxes(title_text = "Equivalent area, (%)")
    fig.update_xaxes(title_text = "Time (s)")
    if show:
        fig.show()
    htmlFnm = fnm.split('.', 2)[0] + 'Solar.html'
    pio.write_html(fig, htmlFnm)
    
    # Plot orbit and difference with SGP4
    fig = make_subplots(rows = 2, cols = 1,
                        shared_xaxes = True,
                        subplot_titles = ("Orbit", " Difference with SGP4"),
                        vertical_spacing = 0.05)
    fig.add_trace(go.Scatter(x = cb.t[idx], y = cb.p[0, idx], mode="lines", showlegend = True, name = 'X'), row = 1, col = 1)
    fig.add_trace(go.Scatter(x = cb.t[idx], y = cb.p[1, idx], mode="lines", showlegend = True, name = 'Y'), row = 1, col = 1)
    fig.add_trace(go.Scatter(x = cb.t[idx], y = cb.p[2, idx], mode="lines", showlegend = True, name = 'Z'), row = 1, col = 1)
    
    fig.add_trace(go.Scatter(x = cb.t[idx], y = cb.p[0, idx] - cb.orb.p[0, idx], mode="lines", showlegend = True, name = 'X-X SGP4'), row = 2, col = 1)
    fig.add_trace(go.Scatter(x = cb.t[idx], y = cb.p[1, idx] - cb.orb.p[1, idx], mode="lines", showlegend = True, name = 'Y-Y SGP4'), row = 2, col = 1)
    fig.add_trace(go.Scatter(x = cb.t[idx], y = cb.p[2, idx] - cb.orb.p[2, idx], mode="lines", showlegend = True, name = 'Z-Z SGP4'), row = 2, col = 1)
    
    fig.layout.update(title = "Position")
    fig.update_yaxes(title_text="Pos., (m)", row = 1, col = 1)
    fig.update_yaxes(title_text="Pos. diff. with SGP4, (m)", row = 2, col = 1)
    fig.update_xaxes(title_text="Time (s)", row = 2, col = 1)
    if show:
        fig.show()
    htmlFnm = fnm.split('.', 2)[0] + 'Pos.html'
    pio.write_html(fig, htmlFnm)
    #pngFnm = fnm.split('.', 2)[0] + 'Pos.png'
    #pio.write_image(fig, pngFnm, 'png')
    
    # Energy
    GMm = cb.GM * cb.cbM.m
    k = cb.k
    kinE = 0.5 * cb.cbM.m * np.linalg.norm(cb.v[:, idx], axis = 0)**2
    dynE =  -GMm / np.linalg.norm(cb.p[:, idx], axis = 0)
    
    kinEsgp4 = 0.5 * cb.cbM.m * np.linalg.norm(cb.orb.v[:, idx], axis = 0) **2
    dynEsgp4 = -GMm / np.linalg.norm(cb.orb.p[:, idx], axis = 0)
    
    fig = make_subplots(rows = 3, cols = 1,
                    shared_xaxes = True,
                    subplot_titles = ("Orbit's kinetic / total energy", "Dynamic energy", "Difference with SGP4"),
                    vertical_spacing = 0.05)
    fig.add_trace(go.Scatter(x = cb.t[idx], y = kinE, mode="lines", showlegend = True, name = 'KinE'), row = 1, col = 1)
    fig.add_trace(go.Scatter(x = cb.t[idx], y = dynE, mode="lines", showlegend = True, name = 'DynE'), row = 2, col = 1)
    fig.add_trace(go.Scatter(x = cb.t[idx], y = -(kinE + dynE), mode="lines", showlegend = True, name = '-TotE'), row = 1, col = 1)
    
    fig.add_trace(go.Scatter(x = cb.t[idx], y = kinEsgp4, mode="lines", showlegend = True, name = 'KinE, SGP4'), row = 1, col = 1)
    fig.add_trace(go.Scatter(x = cb.t[idx], y = dynEsgp4, mode="lines", showlegend = True, name = 'DynE, SGP4'), row = 2, col = 1)
    fig.add_trace(go.Scatter(x = cb.t[idx], y = -(kinEsgp4 + dynEsgp4), mode="lines", showlegend = True, name = '-TotE, SGP4'), row = 1, col = 1)
    
    fig.add_trace(go.Scatter(x = cb.t[idx], y = kinE - kinEsgp4, mode="lines", showlegend = True, name = 'dKinE'), row = 3, col = 1)
    fig.add_trace(go.Scatter(x = cb.t[idx], y = dynE - dynEsgp4, mode="lines", showlegend = True, name = 'dDynE'), row = 3, col = 1)
    fig.add_trace(go.Scatter(x = cb.t[idx], y = kinE + dynE - kinEsgp4 - dynEsgp4, mode="lines", showlegend = True, name = 'dTotE'), row = 3, col = 1)
    
    fig.layout.update(title = "Orbit's energy")
    fig.update_yaxes(title_text="Kin. / Tot. Energy, (J)", row = 1, col = 1)
    fig.update_yaxes(title_text="Dyn. Energy, (J)", row = 2, col = 1)
    fig.update_yaxes(title_text="Diff. with SGP4, (J)", row = 3, col = 1)
    fig.update_xaxes(title_text="Time (s)", row = 3, col = 1)
    
    fig.update_layout(legend_orientation="h")
    if show:
        fig.show()
    htmlFnm = fnm.split('.', 2)[0] + 'Energy.html'
    pio.write_html(fig, htmlFnm)

