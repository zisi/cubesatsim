
import math
import numpy as np
import matplotlib.pyplot as plt
import pyquaternion as pq
from reportlab.lib.styles import LineStyle

class qbPlotter:
    def __init__(self):
        f = plt.gcf()
        ax = []
        ax.append(plt.subplot2grid((2,6), (0,0), 2, 4))
        ax.append(plt.subplot2grid((2,6), (0,4), 1, 2))#, projection='polar'))
        ax.append(plt.subplot2grid((2,6), (1,4), 1, 2))#, projection='polar'))
        f.tight_layout()
        f.set_size_inches(9, 7)
        off = [-90, 0,0]
        R = np.zeros((3,3,3))
        R[0] = np.array([[-1,0,0],[0,0,-1],[0,-1,0]]).transpose() 
        R[1] = np.array([[0,1,0],[1,0,0],[0,0,-1]]).transpose()
        R[2] = np.array([[0,0,-1],[1,0,0],[0,-1,0]]).transpose()
        Rf = np.eye(3)
        self.Rf = Rf
        self.R = R
        self.c = ((0.2,.1,1),(1,1,1),(1,0,0))
        qr = []
        qf = []
        qs = []
        qm = []
        tickOff = [1.05, 1.1, 1.1]
        for i in range(3):
            c1=plt.Circle((0,0),1,fill=False)
            ax[i].add_artist(c1)
            for j in [30,45,80]:
                ctmp = plt.Circle((0,0),1*math.cos(j*math.pi/180), fill=False, color = '0.8', linewidth=0.2)
                ax[i].add_artist(ctmp)
            ax[i].set_xlim(-1.1, 1.1)
            ax[i].set_ylim(-1.1, 1.1)
            ax[i].set_aspect('equal')
            ax[i].axis('off')
            for j in np.linspace(0, 360-30, int(360/30)):
                x = tickOff[i] * math.sin((j+off[i])*math.pi/180)
                y = tickOff[i] * math.cos((j+off[i])*math.pi/180)
                ha = 'center'
                if x > 0.1:
                    ha = 'left'
                elif x < -0.1:
                    ha = 'right'
                ax[i].text(x, y, '${0}^0$'.format(j),
                           horizontalalignment= ha,
                           verticalalignment='center')
                ax[i].plot([0, x/tickOff[i]],[0, y/tickOff[i]], color = '0.8', linewidth = 0.2)
            Rt = R[i] @ Rf
            qr.append(ax[i].quiver([0,0,0],[0,0,0],Rt[0,:], Rt[1,:],linewidth=2, width=0.01,linestyle='dashed',facecolor ='none', edgecolor=['C0','C1','C2'] ,angles='xy', scale_units='xy', scale=1.))
            qf.append(ax[i].quiver([0,0,0],[0,0,0],Rt[0,:], Rt[1,:], color=['C0','C1','C2'] ,angles='xy', scale_units='xy', scale=1.))
            qs.append(ax[i].quiver([0],[0], [0], [1], color=['C8'], linewidth=1, width=0.01, linestyle='dashed',facecolor ='none', edgecolor=['C8'], angles='xy', scale_units='xy', scale=1.))
            qm.append(ax[i].quiver([0],[0], [0], [1], color=['C9'], linewidth=1, width=0.01 ,linestyle='dashed',facecolor ='none', edgecolor=['C9'], angles='xy', scale_units='xy', scale=1.))           
        self.f = f
        self.ax = ax
        self.qf = qf
        self.qs = qs
        self.qm = qm
        ax[0].text(0, -1.1, 'Side View (-X-Z)', horizontalalignment= 'center', verticalalignment = 'top')
        ax[1].text(0, -1.2, 'Top View (YX)',    horizontalalignment= 'center', verticalalignment = 'top')
        ax[2].text(0, -1.2, 'Rear View (Y-Z)',  horizontalalignment= 'center', verticalalignment = 'top')
        f.suptitle('Orbit alignment view')
        plt.ion()
    def updFrame(self, R_LG, sv_LF, magn, title = 'Orbit alignment view', isSunVis = True):
        """ Update satellite alignment plot w.r.t Local orbit frame  
            
            Args:
                R_LG:  Rotation matrix representing Geometry frame w.r.t Local orbit Frame
                sv_LF: Sun vector in local orbit frame
        """
        sunClr = ['k', 'C8'][isSunVis]
        for i in range(3):
            Rt = self.R[i] @ R_LG
            self.qf[i].set_UVC(Rt[0,:], Rt[1,:])
            self.qs[i].set_edgecolor(sunClr)
        self.qs[0].set_UVC(-sv_LF[0], -sv_LF[2])
        self.qs[1].set_UVC( sv_LF[1],  sv_LF[0])
        self.qs[2].set_UVC( sv_LF[1], -sv_LF[2])
                
        self.qm[0].set_UVC(-magn[0], -magn[2])
        self.qm[1].set_UVC( magn[1],  magn[0])
        self.qm[2].set_UVC( magn[1], -magn[2])
        self.f.suptitle(title)
        #self.ax[0].redraw_in_frame()
        self.f.canvas.draw()
        self.f.canvas.flush_events()
        



